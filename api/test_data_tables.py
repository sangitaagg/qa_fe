import json
from util.conn import Connection
from util.config import *
from util.lib import from_dob_to_age,validate_myanmar_nrc,validate_myanmar_phnum
from util.include import *

from pytest import mark,fixture
from api.test_data import client_schema
#from schema import Schema, And, Use, Optional

@fixture(scope = "module")
def test_client_sync(test_setup_backend):
    try:
        session = Connection.get_session()
        response = session.get(clientsync_url, headers=Connection.get_headers(), data={})
        assert response.status_code ==200
        return response
    except Exception as ex:
        logger.log(LOGGING_LEVEL_NOTICE, "Exception {} ".format(ex))
        assert False

def test_create_tables():
    pass

def test_update_table():
    pass

def test_delete_table():
    pass