import json
import yaml
from util.conn import Connection
from util.config import *
from pytest import mark,fixture
from util.include import *

def json_load_data(path):
    with open(path) as login_data:
        data = yaml.load(login_data)
        logger.log(LOGGING_LEVEL_NOTICE,"data= {}".format(data))
        return data

@fixture(scope = "module",params=json_load_data(js_login))
def test_login_data(request):
    test_data = request.param
    logger.log(LOGGING_LEVEL_NOTICE,"test_data {}".format(test_data))
    return test_data

@mark.APIBVT
def test_login(test_login_data):
    try:
        username = test_login_data.get("username")
        password = test_login_data.get("password")
        validate = test_login_data.get("validate")

        logger.log(LOGGING_LEVEL_NOTICE, "username: " + str(username) + " "\
              "password: " + str(password) + " "\
              "validate: " + str(validate) + " "\
              "login_url: " + str(login_url) + " "\
              "headers: " + str(Connection.get_login_headers()))

        payload = 'username=' + username + '&password=' + password + '&grant_type=' + GRANT_TYPE
        session = Connection.get_session()
        response = session.post(login_url, headers=Connection.get_login_headers(), data=payload)

        login_validate(response, validate)
    except Exception as ex:
        logger.log(LOGGING_LEVEL_NOTICE,"Exception {}".format(ex))


def login_validate(response, validate):
    print(response.status_code)
    assert response.status_code == int(validate.get("status_code"))

    out = json.loads(response.text)


    if validate.get("access_token") == "True":
        assert out["access_token"]

    if validate.get("expires_in") == "True":
        assert out["expires_in"]

