from util.conn import Connection
from util.config import *
from util.include import *
from pytest import mark,fixture
import json
from api.test_data import client_schema
#from schema import Schema, And, Use, Optional

@fixture(scope = "module")
def test_client_sync(test_setup_backend):
    try:
        session = Connection.get_session()
        response = session.get(clientsync_url, headers=Connection.get_headers(), data={})
        assert response.status_code ==200
        return response
    except Exception as ex:
        logger.log(LOGGING_LEVEL_NOTICE, "Exception {} ".format(ex))
        assert False

@mark.Debug1
def test_get_client_document(test_client_sync):
    """
    This test is validating client documents in the form of JPEG,png and response status code.
    """
    response = test_client_sync
    try:
        json_obj = json.loads(response.text.encode("utf-8"))
        for d in json_obj.get('data'):
            documents = d.get("documents")
            #TODO : Need to check if document can be empty of not
            if documents is not None:
                for doc_id in documents:
                    doc_url = client_images+ str(d.get("id"))+"/documents/"+str(doc_id.get("id"))
                    print(doc_url)
                    session = Connection.get_session()
                    response = session.get(doc_url, headers=Connection.get_headers(), data={})
                    assert response.status_code == 200
                    #assert str(doc_id.get("type") in ("image/png","image/jpeg"))
                    #TODO need to check the number of images or headers
    except Exception as ex:
        print(ex)
        assert False

@mark.test
def test_upload_client_document_in_jpg_format(test_client_sync):
    """
     Uploading document in JPEG format
    """
    print("I am in document post")
    payload = {'name ': 'Name',
               'description': 'Uploading client doc pic'}
    files = [
        ('file ', ('index.jpg', open('api/test_data/images/tab1.jpg', 'rb'), 'image/jpeg'))
    ]
    doc_url = eos_url +"v1/images/CLIENT/2/documents"
    print("Document URL")
    print(doc_url)
    session = Connection.get_session()
    response = session.get(doc_url, headers=Connection.get_headers(), data=payload,files=files)
    print("Addition -----------")
    print(response.status_code)


@mark.Debug1
def test_delete_client_document(test_client_sync):
    response = test_client_sync
    try:
        json_obj = json.loads(response.text.encode("utf-8"))
        for d in json_obj.get('data'):
            documents = d.get("documents")
            if documents is not None:
                for doc_id in documents:
                    #TODO need to check we need to delete the all the document or some specific
                    '''doc_url = client_images + str(d.get("id")) + "/documents/" + str(doc_id.get("id"))
                    print(doc_url)
                    session = Connection.get_session()
                    res = session.get(doc_url, headers=Connection.get_headers(), data={})
                    assert res.status_code ==200 '''

    except Exception as ex:
        print(ex)
        assert False
