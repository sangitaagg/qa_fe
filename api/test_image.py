import json
from util.conn import Connection
from util.config import *
from util.include import *

from pytest import mark,fixture
from api.test_data import client_schema
#from schema import Schema, And, Use, Optional

@fixture(scope = "module")
def test_client_sync(test_setup_backend):
    try:
        session = Connection.get_session()
        response = session.get(clientsync_url, headers=Connection.get_headers(), data={})
        assert response.status_code ==200
        return response
    except Exception as ex:
        logger.log(LOGGING_LEVEL_NOTICE, "Exception {} ".format(ex))
        assert False

@mark.APIBVT
def test_get_client_image(test_client_sync):
    """
    Downloading the client image, validate the status if image is present and not present
    Validating the status code from finreact and DFA both in case client image present or not present on server
    """

    response = test_client_sync
    try:
        json_obj = json.loads(response.text.encode("utf-8"))
        for d in json_obj.get('data'):
            image_url = client_images + str(d.get("id")) + "?maxHeight=150"
            logger.log(LOGGING_LEVEL_NOTICE, image_url)
            session = Connection.get_session()
            res = session.get(image_url, headers=Connection.get_headers(), data={})
            if d.get("imagePresent")== "True" :
               assert res.status_code == 200
               assert "image" in res.text
            elif d.get("imagePresent") == "False" :
                logger.log(LOGGING_LEVEL_NOTICE,"Image is not present for the client {}".format(d.get("id")))
                if res.status_code ==502 : #this status code is from DFA
                    image_obj = json.loads(res.text.encode("utf-8"))
                    for err in image_obj.get("errors"):
                        assert err.get("code") == '404' #Finreact error code image not present
                        assert  err.get("message") == "Image for resource clients with Identifier "+str(d.get("id"))+" does not exist"
            else :
                logger.log(LOGGING_LEVEL_NOTICE,"ImagePresent field is not returning true or false {}".format(d.get("imagePresent")))

    except Exception as ex:
        logger.log(LOGGING_LEVEL_NOTICE,"Exception : ".format(ex))
        assert False

