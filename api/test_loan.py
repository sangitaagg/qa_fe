import json
from util.conn import Connection
from util.config import *
from util.include import *
from pytest import mark,fixture
from api.test_data import client_schema
#from schema import Schema, And, Use, Optional

@fixture(scope = "module")
def test_client_sync(test_setup_backend):
    try:
        session = Connection.get_session()
        response = session.get(clientsync_url, headers=Connection.get_headers(), data={})
        assert response.status_code ==200
        return response
    except Exception as ex:
        logger.log(LOGGING_LEVEL_NOTICE, "Exception {} ".format(ex))
        assert False

@mark.Debug
def test_client_loan(test_client_sync):
    """
    This test is validating all the number of loans for the client.
    Loan API status,Total totalRepaymentExpected = totalPrincipalDisbursed+totalInterestCharged+fees
    totalOutstanding = totalRepaymentExpected - totalPaidInAdvance
    Num of periods = numberOfRepayments+1
    Todo : need to verify Arrears functionality
    """
    response = test_client_sync
    try:
        json_obj = json.loads(response.text.encode("utf-8"))
        for d in json_obj.get('data'):
            loan = d.get("loans")
            if loan is not None :
                for loan_id in loan:
                    loan_url = eos_url +"v1/loans/"+str(loan_id.get("id"))
                    logger.log(LOGGING_LEVEL_NOTICE, loan_url)
                    session = Connection.get_session()
                    res = session.get(loan_url, headers=Connection.get_headers(), data={})
                    assert res.status_code == 200
                    json_obj = json.loads(res.text.encode("utf-8"))
                    logger.log(LOGGING_LEVEL_NOTICE,"Loan api res {}".format(str(json_obj).encode("utf8")))

                    principal = loan_id["repaymentSchedule"]["totalPrincipalDisbursed"]
                    interst = loan_id["repaymentSchedule"]["totalInterestCharged"]
                    fees =  loan_id["repaymentSchedule"]["totalFeeChargesCharged"]
                    total_amount = principal+fees+interst
                    assert loan_id["repaymentSchedule"]["totalRepaymentExpected"] == total_amount
                    payment= loan_id["repaymentSchedule"]["totalRepaymentExpected"]
                    adv_pay = loan_id["repaymentSchedule"]["totalPaidInAdvance"]
                    out_amt = loan_id["repaymentSchedule"]["totalOutstanding"]
                    assert out_amt == payment - adv_pay
                    #print(loan_id.get("inArrears"))
                    num_of_periods = loan_id.get("numberOfRepayments")+1
                    #print(num_of_periods)
                    assert num_of_periods == len(loan_id["repaymentSchedule"]["periods"])
                    if loan_id.get("inArrears") == False:
                        pass
                    else:
                        pass

                    #TODO need to check in case of Arrears is true
                    #TODO need to check on number of loans
                    #TODO Check for the loan amount gretaer than 5 Million than need CM approval.

    except Exception as ex:
        print(ex)
        assert False

@mark.BVT
def test_client_Summary_loan_amt(test_client_sync):
    """
    Validate clientSummary[loanAmount] should be equal to totalRepaymentExpected of the latest loan. Compare Period 0 due date with the latest one
    TODO : Need to picked the recent loan
    """
    response = test_client_sync

    try:
        json_obj = json.loads(response.text.encode("utf-8"))

        for d in json_obj.get('data'):

            if  d["clientSummary"]["loanAmount"] != 0.0:
                print(d["clientSummary"]["loanAmount"])
                loan = d.get("loans")
                for loan_id in loan:
                    #TODO : Need to compare the dates and picked the latest due date
                    loan_date = str(loan_id["repaymentSchedule"]["periods"][0]["dueDate"])
                    print(loan_date)

    except Exception as ex:
        print(ex)
        assert False

