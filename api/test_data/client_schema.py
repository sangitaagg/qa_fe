from schema import Schema, And, Use, Optional
from datetime import datetime

test_client_schema = Schema({
  "data": [
    {
      "id": int,
      "accountNo": str,
      "clientStatus": And(str,lambda s: s in ("INVALID","PENDING","ACTIVE","TRANSFER_IN_PROGRESS","TRANSFER_ON_HOLD","CLOSED","REJECTED","WITHDRAWN")),
      Optional("activationDate"): object, #Its not optional field
      "firstname": str,
      "lastname": str,
      "displayName": str,
        Optional('dateOfBirth'): (object), #datetime.strftime('%Y-%m-%d'), #optional field
        Optional("genderType"): And(str,lambda gt : gt in ("Male","Female")),
        "officeId": int,
        "officeName":str,
        Optional("timeline") : object,
        Optional("clientDetails"):object,
        Optional("householdInfo") : object,
        Optional("currentAssets") :object,
        Optional("houseInfo") : object,
        Optional("obligations"): object,
        Optional("businessInfo") : object,
        Optional("cashFlow") : object,
        Optional("documents") : object,
        Optional("fixedAssets") : object,
        Optional("employeeGroups") : object,
        Optional("recentLoans") : object,
        Optional("licenses") : object,
        Optional("localAuthorityKYCs") : object,
        Optional("clientSummary") : object,
        Optional("loans"): object,
        Optional("savingAccounts"): object,
        "staff":	bool,
        "imagePresent":	bool
    }
    ],
          Optional("status") : int,
          Optional("message") : str,
          Optional("errors"): object
})
