import json
from util.conn import Connection
from util.config import *
from util.lib import from_dob_to_age,validate_myanmar_nrc,validate_myanmar_phnum
from util.include import *

from pytest import mark,fixture
from api.test_data import client_schema
#from schema import Schema, And, Use, Optional

@fixture(scope = "module")
def test_client_sync(test_setup_backend):
    try:
        session = Connection.get_session()
        response = session.get(clientsync_url, headers=Connection.get_headers(), data={})
        assert response.status_code ==200
        return response
    except Exception as ex:
        logger.log(LOGGING_LEVEL_NOTICE, "Exception {} ".format(ex))
        assert False

@mark.APIBVT
def test_client_sync_schema(test_client_sync):
    """
    This test is validating status code,message and error of client sync api
    #TODO : Need to add the schema validation par if needed
    """
    try:
        response = test_client_sync
        json_obj = json.loads(response.text.encode("utf-8"))
        assert json_obj.get('message') == "Clients synced successfully"
        assert json_obj.get('error') == None
        #TODO : Need to explore how to validate schema
        #validated = client_schema.test_client_schema.validate(json_obj)
        logger.log(LOGGING_LEVEL_NOTICE,client_schema.test_client_schema.is_valid(json_obj))
    except Exception as ex:
        logger.log(LOGGING_LEVEL_NOTICE,"Exception {} ".format(ex))
        assert False

@mark.APIBVT
def test_client_id(test_client_sync):
    """
        API : v1/clients/clientid
        This test validate all the clients mandatory fields should not  be null
        Age  : Greater than 18, Gender : (Male, Female), National ID : Both client and spouse in myanmar format
        phone : Both client and spouse,
        Education,Married status.
        #Todo
    """
    response = test_client_sync

    json_obj = json.loads(response.text.encode("utf-8"))
    for d in json_obj.get('data'):
        try:
            if d.get("id"):
                client_url= eos_url +"v1/clients/"+str(d.get("id"))
                logger.log(LOGGING_LEVEL_NOTICE,client_url)
                session = Connection.get_session()
                response = session.get(client_url, headers=Connection.get_headers(), data={})
                assert response.status_code == 200

                json_obj=json.loads(response.text)
                if not json_obj["data"]["accountNo"]:
                    assert False
                if not json_obj["data"]["clientStatus"]:
                    assert False
                #if not json_obj["data"]["activationDate"]: #If the client is not approved just submitted
                #    assert False
                if not json_obj["data"]["firstname"]:
                    assert False
                if not json_obj["data"]["lastname"]:
                    assert False
                if not json_obj["data"]["displayName"]:
                    assert False
                if not json_obj["data"]["officeId"]:
                    assert False
                if not json_obj["data"]["officeName"]:
                    assert False
                if json_obj["data"]["dateOfBirth"]:
                    age=json_obj["data"]["dateOfBirth"]
                    current_age=from_dob_to_age(age)
                    logger.log(LOGGING_LEVEL_NOTICE,"age={}".format(current_age))
                    assert current_age >= 18
                if json_obj["data"]["genderType"]:
                    gender = json_obj["data"]["genderType"]
                    assert gender in ("Female","Male")
                if json_obj["data"]["sduiData"]:
                    if json_obj["data"]["sduiData"]["Client Information"]:
                        for client_info in  json_obj["data"]["sduiData"]["Client Information"]:
                            national_id =client_info.get("National ID Number")
                            spouse_national_id = client_info.get("National ID Number of the Spouse")
                            logger.log(LOGGING_LEVEL_NOTICE,"National id {}".format(current_age))
                            assert validate_myanmar_nrc(national_id)
                            assert validate_myanmar_nrc(spouse_national_id)
                            ph_num = client_info.get("Emergency Phone Number")
                            logger.log(LOGGING_LEVEL_NOTICE, "Phone number {}".format(ph_num))
                            assert validate_myanmar_phnum(ph_num)  #Need to check if ph validation is proper
                            sp_ph_num = client_info.get("Phone Number of the Spouse")
                            logger.log(LOGGING_LEVEL_NOTICE, "Phone number {}".format(ph_num))
                            assert validate_myanmar_phnum(sp_ph_num)
                            edu_level = client_info.get("Education Level")
                            assert edu_level in ("Elementary","Middle","High","Grad","Postgrad","Vocational","None","Other")
                            mari_satus = client_info.get("Marital Status")
                            assert mari_satus in ("Single","Married","Widow_er","Separated","Divorced")
        except Exception as ex:
            logger.log(LOGGING_LEVEL_NOTICE,"Exception {} ".format(ex))
            assert False

@mark.debug1
def test_client_Summary_aggregate_Status(test_client_sync):
    """
        This test validate the clientAggregatedStatus field should match with clientStatus filed
        TODO: This part required more refinement by dev team
    """
    response = test_client_sync

    try:
        json_obj = json.loads(response.text.encode("utf-8"))
        for d in json_obj.get('data'):
            #Need to add the logging to check the validation part
           if d["clientStatus"] in ("PENDING","REJECTED","TRANSFER_IN_PROGRESS","CLOSED","WITHDRAWN","TRANSFER_ON_HOLD","INVALID") :
                assert d["clientStatus"] == d["clientSummary"]["clientAggregatedStatus"]
           elif d["clientStatus"] == "ACTIVE":
                assert d["clientSummary"]["clientAggregatedStatus"] in  ("CLOSED_OBLIGATIONS_MET","LOAN_ACTIVE","OVERDUE_PAYMENT","ACTIVE","APROVED")
           else:
                print("ClientStatus field is not matching client aggregate status")

    except Exception as ex:
        print(ex)
        assert False


@mark.BVT
def test_house_hold_info(test_client_sync):
    """
        This test validate house hold info field should not be negative
        It should be zero\greater then zero\null
    """
    response = test_client_sync
    try:
        json_obj = json.loads(response.text.encode("utf-8"))
        for d in json_obj.get('data'):
            house_Info = d.get("householdInfo")
            print(house_Info)
            if house_Info :
                assert int(house_Info.get("landHouseRentIncome")) >=0.0
                assert int(house_Info.get("agricultureLivestockIncome")) >=0.0
                assert int(house_Info.get("totalRemittanceReceived")) >= 0.0
                assert int(house_Info.get("sumOfAllSalariesReceived")) >= 0.0
                assert int(house_Info.get("foodExpenses")) >= 0.0
                assert int(house_Info.get("landHouseRent")) >= 0.0
                assert int(house_Info.get("electricityBill")) >= 0.0
                assert int(house_Info.get("waterBill")) >= 0.0
                assert int(house_Info.get("clothingExpenses")) >= 0.0
                assert int(house_Info.get("healthExpenses")) >= 0.0
                assert int(house_Info.get("educationExpenses")) >= 0.0
                assert int(house_Info.get("telephoneExpenses")) >= 0.0
                assert int(house_Info.get("donationsGiven")) >= 0.0

    except Exception as ex :
        print(ex)
        assert False

