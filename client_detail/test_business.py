import pytest
from appium.webdriver.common.touch_action import TouchAction
from pytest import mark
from pytest import fixture
import time

@fixture(scope= "module")
def test_client_setup(test_setup):
    driver = test_setup
    i=1

    path = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/androidx.cardview.widget.CardView[" + str(i) + "]/android.view.ViewGroup"
    driver.find_element_by_xpath(path).click()

    time.sleep(1)
    business_path = "//android.widget.LinearLayout[@content-desc=\"Business\"]/android.widget.TextView"


    print(business_path)
    driver.find_element_by_xpath(business_path).click()
    time.sleep(2)

#@mark.busin
def test_business_info(test_setup,test_client_setup):
    driver = test_setup
    busin_photo= driver.find_element_by_id("clientPhotosTextView").text
    print(busin_photo)
    busi_info= driver.find_element_by_id("formHeaderTextView").text
    print(busi_info)
    xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.widget.ScrollView/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/"

    type_of_income= xpath+"android.view.ViewGroup[2]/android.widget.TextView[1]"
    value_income_type= xpath+"android.view.ViewGroup[2]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(type_of_income).text)
    print(driver.find_element_by_xpath(value_income_type).text)

    driver.swipe(100, 700, 100, 150)

    propose_business = xpath + "android.view.ViewGroup[3]/android.widget.TextView[1]"
    value_propose_business = xpath + "android.view.ViewGroup[3]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(propose_business).text)
    print(driver.find_element_by_xpath(value_propose_business).text)

    start_business= xpath+"android.view.ViewGroup[4]/android.widget.TextView[1]"
    value_start_business = xpath+"android.view.ViewGroup[4]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(start_business).text)
    print(driver.find_element_by_xpath(value_start_business).text)

    estab_date=xpath+"android.view.ViewGroup[5]/android.widget.TextView[1]"
    value_estab_date = xpath+"android.view.ViewGroup[5]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(estab_date).text)
    print(driver.find_element_by_xpath(value_estab_date).text)

    land_owner = xpath+"android.view.ViewGroup[6]/android.widget.TextView[1]"
    value_of_land_owner = xpath+"android.view.ViewGroup[6]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(land_owner).text)
    print(driver.find_element_by_xpath(value_of_land_owner).text)

    busi_loc= xpath+"android.view.ViewGroup[7]/android.widget.TextView[1]"
    value_busi_loc = xpath+"android.view.ViewGroup[7]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(busi_loc).text)
    print(driver.find_element_by_xpath(value_busi_loc).text)

    num_income= xpath+"android.view.ViewGroup[8]/android.widget.TextView[1]"
    value_num_income =xpath+ "android.view.ViewGroup[8]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(num_income).text)
    print(driver.find_element_by_xpath(value_num_income).text)

    name_of_income = xpath+"android.view.ViewGroup[9]/android.widget.TextView[1]"
    value_of_income= xpath+"android.view.ViewGroup[9]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(name_of_income).text)
    print(driver.find_element_by_xpath(value_of_income).text)

#@mark.busin
def test_license_fields(test_setup,test_client_setup):
    driver = test_setup
    driver.swipe(200, 1100, 200, 150)
    #TODO need to explore in more detail about the partial scrolling
    xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.widget.ScrollView/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/"

    licen_header = xpath + "android.view.ViewGroup[10]/android.widget.TextView"
    print(driver.find_element_by_xpath(licen_header).text)

    license_validity = xpath + "android.view.ViewGroup[11]/android.widget.TextView[1]"
    value_of_lic_validity = xpath + "android.view.ViewGroup[11]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(license_validity).text)
    print(driver.find_element_by_xpath(value_of_lic_validity).text)

    vehicle_lic_validity = xpath + "android.view.ViewGroup[12]/android.widget.TextView[1]"
    value_of_veh_lic_validity = xpath + "android.view.ViewGroup[12]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(vehicle_lic_validity).text)
    print(driver.find_element_by_xpath(value_of_veh_lic_validity).text)

    pass


def test_num_of_workers(test_setup,test_client_setup):
    driver = test_setup
    driver.swipe(200, 1100, 200, 150)
    #TODO will explore later

@mark.busin
def test_monthly_Cash_flow(test_setup,test_client_setup):
    driver = test_setup
    driver.swipe(200, 1200, 200, 100)
    xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.widget.ScrollView/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/"
    cash_flow = xpath + "android.view.ViewGroup[5]/android.widget.TextView"
    print(driver.find_element_by_xpath(cash_flow).text)
    sales = xpath + "android.view.ViewGroup[6]/android.widget.TextView[1]"
    value_of_sale= xpath + "android.view.ViewGroup[6]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(sales).text)
    print(driver.find_element_by_xpath(value_of_sale).text)

    cost = xpath + "android.view.ViewGroup[7]/android.widget.TextView[1]"
    value_cost= xpath + "android.view.ViewGroup[7]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(cost).text)
    print(driver.find_element_by_xpath(value_cost).text)


#@mark.busin
def test_expenses(test_setup,test_client_setup):
    driver = test_setup
    driver.swipe(200, 1200, 200, 100)

    xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.widget.ScrollView/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/"
    expense =  xpath + "android.view.ViewGroup[8]/android.widget.TextView"
    print(driver.find_element_by_xpath(expense).text)

    sal = xpath + "android.view.ViewGroup[9]/android.widget.TextView[1]"
    value_of_sal= xpath + "android.view.ViewGroup[9]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(sal).text)
    print(driver.find_element_by_xpath(value_of_sal).text)

    rental_fees = xpath + "android.view.ViewGroup[10]/android.widget.TextView[1]"
    value_rental_fees = xpath + "android.view.ViewGroup[10]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(rental_fees).text)
    print(driver.find_element_by_xpath(value_rental_fees).text)

    transport_cost = xpath + "android.view.ViewGroup[11]/android.widget.TextView[1]"
    value_of_transport = xpath + "android.view.ViewGroup[11]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(transport_cost).text)
    print(driver.find_element_by_xpath(value_of_transport).text)

    other_expense = xpath + "android.view.ViewGroup[12]/android.widget.TextView[1]"
    value_transport= xpath + "android.view.ViewGroup[12]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(other_expense).text)
    print(driver.find_element_by_xpath(value_transport).text)