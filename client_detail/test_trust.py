import pytest
from appium.webdriver.common.touch_action import TouchAction
from pytest import mark
from pytest import fixture
import time

@fixture(scope= "module")
def test_client_setup(test_setup):
    driver = test_setup
    i=1

    path = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/androidx.cardview.widget.CardView[" + str(i) + "]/android.view.ViewGroup"
    driver.find_element_by_xpath(path).click()

    time.sleep(1)

    trust_path = "//android.widget.LinearLayout[@content-desc=\"Trust\"]/android.widget.TextView"
    print(trust_path)
    driver.find_element_by_xpath(trust_path).click()
    time.sleep(1)

@mark.trust
def test_get_trust_data(test_setup,test_client_setup):
    driver = test_setup
    print("Hello world")
    date = driver.find_element_by_id("firstHeaderTextView").text
    date_value = driver.find_element_by_id("dateTextView").text
    print(date)
    print(date_value)
    title = driver.find_element_by_id("secondHeaderTextView").text
    titile_value = driver.find_element_by_id("titleTextView").text
    print(title)
    print(titile_value)

    opinion = driver.find_element_by_id("thirdHeaderTextView").text
    opinion_value= driver.find_element_by_id("opinionTextView").text
    print(opinion)
    print(opinion_value)

