import pytest
from appium.webdriver.common.touch_action import TouchAction
from pytest import mark
from pytest import fixture
import time

@fixture(scope= "module")
def test_client_setup(test_setup):
    driver = test_setup
    i=1
    #xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.widget.ScrollView/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/"
    path = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/androidx.cardview.widget.CardView[" + str(i) + "]/android.view.ViewGroup"
    driver.find_element_by_xpath(path).click()
    time.sleep(1)
    #return xpath

#@mark.ClientDetail1
def test_get_client_info_in_detail(test_setup,test_client_setup):
    driver = test_setup

    client_name = driver.find_elements_by_id("clientNameTextView")[0].text
    print("Client Name")
    print(client_name)
    client_number = driver.find_elements_by_id("clientPhoneNumberTextView")[0].text
    print(client_number)
    client_address = driver.find_elements_by_id("clientAddressTextView")[0].text
    print(client_address)
    infomation = driver.find_elements_by_id("informationLabelTextView")[0].text
    print("Informaton Lable")
    print(infomation)
    #xpath = "// android.widget.LinearLayout[ @ content - desc = "Household"] / android.widget.TextView"
    photos = driver.find_elements_by_id("clientPhotosTextView")[0].text
    print("Photos")
    print(photos)

    #TODO display the client name
    #assert client_name == ""

    #Client_basic_info=  driver.find_elements_by_id("formHeaderTextView").text
    #Client_basic_info == "Basic Client Info"

#@mark.ClientDetail
def test_client_basic_info(test_setup, test_client_setup):
    driver = test_setup

    xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.widget.ScrollView/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/"
    name_path = xpath + "android.view.ViewGroup[2]/android.widget.TextView[1]"
    print(driver.find_element_by_xpath(name_path).text)
    value_name_path = xpath + "android.view.ViewGroup[2]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(value_name_path).text)
    """
        Touch :
        TouchAction(driver)   .press(x=317, y=605)   .move_to(x=317, y=605)   .release()   .perform()
        touch = TouchAction(driver)
        touch.press(x=293, y=948).move_to(x=313, y=521).release().perform()

    """

    driver.swipe(100, 700, 100, 150)

    citi_num= xpath+ "android.view.ViewGroup[3]/android.widget.TextView[1]"
    value_citi_num = xpath + "android.view.ViewGroup[3]/android.widget.TextView[2]"

    print(driver.find_element_by_xpath(citi_num).text)
    print(driver.find_element_by_xpath(value_citi_num).text)

    phone_num= xpath +"android.view.ViewGroup[4]/android.widget.TextView[1]"
    value_phone_num = xpath + "android.view.ViewGroup[4]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(phone_num).text)
    print(driver.find_element_by_xpath(value_phone_num).text)

    address = xpath +"android.view.ViewGroup[5]/android.widget.TextView[1]"
    value_of_address = xpath +"android.view.ViewGroup[5]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(address).text)
    print(driver.find_element_by_xpath(value_of_address).text)

    branch =  xpath+"android.view.ViewGroup[6]/android.widget.TextView[1]"
    branch_value = xpath+"android.view.ViewGroup[6]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(branch).text)
    print(driver.find_element_by_xpath(branch_value).text)

@mark.ClientDetail
def test_cleint_detail_info(test_setup,test_client_setup):
    driver = test_setup
    #xpath = test_client_setup
    xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.widget.ScrollView/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/"
    #client_detail=driver.find_elements_by_id("formHeaderTextView").text
    #print(client_detail)
    driver.swipe(200, 1100, 200, 150)

    date_of_birth= xpath + "android.view.ViewGroup[7]/android.widget.TextView[1]"
    value_date_of_birth= xpath+"android.view.ViewGroup[7]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(date_of_birth).text)
    print(driver.find_element_by_xpath(value_date_of_birth).text)

    sex = xpath +"android.view.ViewGroup[8]/android.widget.TextView[1]"
    sex_value = xpath + "android.view.ViewGroup[8]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(sex).text)
    print(driver.find_element_by_xpath(sex_value).text)

    father_name= xpath +"android.view.ViewGroup[9]/android.widget.TextView[1]"
    father_name_value= xpath + "android.view.ViewGroup[9]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(father_name).text)
    print(driver.find_element_by_xpath(father_name_value).text)

    yr_of_living = xpath +"android.view.ViewGroup[10]/android.widget.TextView[1]"
    value_yr_of_living = xpath + "android.view.ViewGroup[10]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(yr_of_living).text)
    print(driver.find_element_by_xpath(value_yr_of_living).text)

    edu_status = xpath +"android.view.ViewGroup[11]/android.widget.TextView[1]"
    value_edu= xpath +"android.view.ViewGroup[11]/android.widget.TextView[1]"
    print(driver.find_element_by_xpath(edu_status).text)
    print(driver.find_element_by_xpath(value_edu).text)

    driver.swipe(200, 1100, 200, 150)

    m_status = xpath + "android.view.ViewGroup[12]/android.widget.TextView[1]"
    value_m_status= xpath + "android.view.ViewGroup[12]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(m_status).text)
    print(driver.find_element_by_xpath(value_m_status).text)

    num_of_family= xpath +"android.view.ViewGroup[12]/android.widget.TextView[1]"
    value_family=xpath +"android.view.ViewGroup[12]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(num_of_family).text)
    print(driver.find_element_by_xpath(value_family).text)

    num_working = xpath +"android.view.ViewGroup[13]/android.widget.TextView[1]"
    value_working=xpath +"android.view.ViewGroup[13]/android.widget.TextView[2]"
    print(driver.find_element_by_xpath(num_working).text)
    print(driver.find_element_by_xpath(value_working).text)


