import pytest
from appium.webdriver.common.touch_action import TouchAction
from pytest import mark
from pytest import fixture
import time

@fixture(scope= "module")
def test_guarantor(test_setup):
    driver = test_setup
    i=1

    path = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/androidx.cardview.widget.CardView[" + str(i) + "]/android.view.ViewGroup"
    driver.find_element_by_xpath(path).click()

    time.sleep(1)
    guarantor_path = "//android.widget.LinearLayout[@content-desc=\"Guarantors\"]/android.widget.TextView"
    driver.find_element_by_xpath(guarantor_path).click()

    time.sleep(2)

def test_guarantor_user():
    pass
