from pytest import mark
from util.include import *

@mark.search
def test_display_screen(test_setup):
    """ Validate UI of search box """
    try:
        driver = test_setup
        seach_text = driver.find_element_by_id("searchEditText").text
        assert seach_text == "Search by Name, Phone No, National ID, or Address"
    except Exception as ex:
        logger.log(LOGGING_LEVEL_NOTICE,"Exception {}".format(ex))

@mark.search
def test_remove_search(test_setup):
    """ Validate cleanup button in seacrh box i.e cross symbol """

    driver = test_setup
    try:
        srch_text=driver.find_element_by_id("searchEditText")
        srch_text.send_keys("Doe")
        assert srch_text.text == "Doe"
        driver.find_element_by_id("closeImageView").click()
        assert srch_text.text == "Search by Name, Phone No, National ID, or Address"
    except Exception as ex :
        logger.log(LOGGING_LEVEL_NOTICE, "Exception {}".format(ex))

@mark.debug1
def test_search_by_name(test_setup):
    """ search by name validation
    :param test_setup:
    :return:  It should return all the clients with the search
    """
    driver = test_setup
    try:
        search_byname = "Jeet"
        srch_text = driver.find_element_by_id("searchEditText")

        srch_text.send_keys(search_byname)
        driver.find_element_by_id("searchEditText").click()

        # click on search button from Android keyboard
        driver.execute_script("mobile: performEditorAction", {'action': 'search'})
        #todo need to add the validation
        #assert
    except Exception as ex:
        logger.log(LOGGING_LEVEL_NOTICE, "Exception {}".format(ex))

def test_search_by_ph_num(test_setup):
    """ Search by phone number
    :return: All the clients with searched phone number
    """
    driver = test_setup
    search_by_ph = "97346"
    search_field(driver,search_by_ph)

def test_search_by_national_id(test_setup):
    driver = test_setup
    search_by_national_id = "Laka"
    search_field(driver,search_by_national_id)


def search_field(driver,searchtext):
    """
    This function will accept the search argument and return the
    :return:
    """
    try:
        srch_text = driver.find_element_by_id("searchEditText")

        srch_text.send_keys(searchtext)
        driver.find_element_by_id("searchEditText").click()

        # click on search button from Android keyboard
        driver.execute_script("mobile: performEditorAction", {'action': 'search'})
        # todo need to add the validation
    except Exception as ex:
        logger.log(LOGGING_LEVEL_NOTICE, "Exception {}".format(ex))
