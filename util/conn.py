import requests
import json
from util.config import *

class Connection:
    _access_token=None
    _session=None

    @staticmethod
    def set_token(response):
        try:
            body_json = json.loads(response.text)
            Connection._access_token = "Bearer " + str(body_json.get('access_token'))
            print("login token ------")
            print(Connection._access_token)
        except Exception as ex:
            print(ex)

    @staticmethod
    def get_login_headers():
        headers = {
            'Content-Type': CONTENT_TYPE,
            'Authorization': AUTHORIZATION
        }
        return headers

    @staticmethod
    def get_headers():
        headers = {
            'Content-Type': CONTENT_TYPE,
            'Authorization': Connection._access_token
            #'Accept': 'application/json'
        }
        return headers

    @staticmethod
    def get_session():
        if Connection._session == None:
            Connection._session = requests.session()
            #Connection._session.config['keep_alive'] = False

        return Connection._session



