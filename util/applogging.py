import sys
import logging

LOGGING_LEVEL_NOTICE = logging.INFO+3
logging.addLevelName(LOGGING_LEVEL_NOTICE,'NOTICE')

formatter = logging.Formatter('%(asctime)s %(name)-12s %(filename)s:%(funcName)s:%(lineno)s %(levelname)-8s:%(message)s')

filehandler = logging.FileHandler(r".\logs\app.log")
filehandler.setLevel(LOGGING_LEVEL_NOTICE)
filehandler.setFormatter(formatter)

stdoutHandler = logging.StreamHandler(sys.stdout)
#stdoutHandler.setLevel(LOGGING_LEVEL_NOTICE)
stdoutHandler.setLevel(logging.DEBUG)
stdoutHandler.setFormatter(formatter)

logger = logging.getLogger()
#logger.addHandler(stdoutHandler)
logger.addHandler(filehandler)
logger.setLevel(logging.DEBUG)