import pytest
from pytest import mark

@mark.login
def test_screen_display(test_setup):
    driver = test_setup
    stmt = driver.find_element_by_id("co.gojo.eos:id/textView").text
    if "Sign in to DFA" in stmt:
        print("Sign in to DFA is present")
    else:
        print("Sign in to DFA is not present")

    cont_stmt = driver.find_element_by_id("co.gojo.eos:id/contactSupportTextView").text
    if "Contact Support" in cont_stmt:
        print("contact Support is present")
    else:
        print("Contact support is not present")

@mark.login
def test_eye_button(test_setup):
    driver = test_setup
    driver.find_element_by_id("co.gojo.eos:id/editTextUserName").click()
    search_name = driver.find_element_by_id("co.gojo.eos:id/editTextUserName")
    search_name.send_keys("sangita")

    driver.find_element_by_id("co.gojo.eos:id/editTextPassword").click()
    search_password = driver.find_element_by_id("co.gojo.eos:id/editTextPassword")
    search_password.send_keys("test")

    driver.find_element_by_id("co.gojo.eos:id/passwordToggleImageView").click()
    password = driver.find_element_by_id("co.gojo.eos:id/editTextPassword").text

    assert password == "test"
    #if "test" in password:
    #    print("Toggle button is working")

    driver.find_element_by_id("co.gojo.eos:id/passwordToggleImageView").click()
    password = driver.find_element_by_id("co.gojo.eos:id/editTextPassword").text
    print(password)
    assert "...." in password
    #assert password == "...."
    #if ("...." in password):
    #    print("Password is not displaying")


@pytest.mark.login
def test_invalid_user(test_setup):
    driver = test_setup
    driver.find_element_by_id("co.gojo.eos:id/editTextUserName").click()
    search_name=driver.find_element_by_id("co.gojo.eos:id/editTextUserName")
    search_name.send_keys("sangita")

    driver.find_element_by_id("co.gojo.eos:id/editTextPassword").click()
    search_password = driver.find_element_by_id("co.gojo.eos:id/editTextPassword")
    search_password.send_keys("test")

    driver.find_element_by_id("co.gojo.eos:id/buttonTextView").click()

    if(driver.find_element_by_id("co.gojo.eos:id/loginErrorTextView")):
        print("Invalid login")
        text=driver.find_element_by_id("co.gojo.eos:id/loginErrorTextView").text
        print(text)
        #TODO need to check the correct message

@mark.login
def test_valid_login(test_setup):
    driver = test_setup

    driver.find_element_by_id("co.gojo.eos:id/editTextUserName").click()
    search_name = driver.find_element_by_id("co.gojo.eos:id/editTextUserName")
    search_name.send_keys("sangita")

    driver.find_element_by_id("co.gojo.eos:id/editTextPassword").click()
    search_password = driver.find_element_by_id("co.gojo.eos:id/editTextPassword")
    search_password.send_keys("sangita")

    driver.find_element_by_id("co.gojo.eos:id/buttonTextView").click()