import pytest
from appium.webdriver.common.touch_action import TouchAction
from pytest import mark
from pytest import fixture
import time
from util.conn import Connection
from util.config import *
import json
from util.include import *

@mark.member
def test_mem_active_client(test_setup,test_setup_backend):
    driver = test_setup
    try:
        display_client_info  = test_get_the_client_sync_api_res(test_setup_backend)
        print("Count the element in the list")

        count = 0
        for j in range(0,1):
            memlist = driver.find_elements_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/androidx.cardview.widget.CardView")

            print("len: ")
            print(len(memlist))

            for i in range(0,len(memlist)):
                logger.log(LOGGING_LEVEL_NOTICE,"-----------------------------------------------------------")

                path = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/androidx.cardview.widget.CardView["+str(i+1)+"]/android.view.ViewGroup/"

                api_display_name = str(display_client_info[count].get("displayName")).encode('utf8')
                app_display_name = ui_get_field(driver, path, 1)
                if api_display_name == app_display_name :
                    logger.log(LOGGING_LEVEL_NOTICE,"PASS {}={}".format(api_display_name,app_display_name))
                else:
                    logger.log(LOGGING_LEVEL_NOTICE,"FAIL {} not ={}".format(api_display_name,app_display_name))


                api_office_name = str(display_client_info[count].get("officeName")).encode('utf8')
                app_office_name=ui_get_field(driver, path, 2)
                if (api_office_name == app_office_name):
                    logger.log(LOGGING_LEVEL_NOTICE, "PASS {}={}".format(api_office_name, app_office_name))
                else:
                    logger.log(LOGGING_LEVEL_NOTICE, "FAIL {} not ={}".format(api_office_name, app_office_name))

                api_officer_name = str(display_client_info[count].get("Officer Name")).encode('utf8')
                app_officer_name = ui_get_field(driver, path, 3)
                if (api_officer_name == app_officer_name):
                    logger.log(LOGGING_LEVEL_NOTICE, "PASS api {}={}".format(api_officer_name, app_officer_name))
                else:
                    logger.log(LOGGING_LEVEL_NOTICE, "FAIL {} not ={}".format(api_officer_name, app_officer_name))

                api_status = str(display_client_info[count].get("Status"))
                app_status = driver.find_element_by_xpath(path + "android.view.ViewGroup/android.widget.TextView").text
                if (api_status == app_status):
                    logger.log(LOGGING_LEVEL_NOTICE, "PASS {}={}".format(api_status, app_status))
                else:
                    logger.log(LOGGING_LEVEL_NOTICE, "FAIL {} not ={}".format(api_status, app_status))

                api_loan_amt = str(display_client_info[count].get("loanAmount")).encode('utf8')
                app_loan_amt = ui_get_field(driver, path, 4)
                if ( api_loan_amt == app_loan_amt):
                    logger.log(LOGGING_LEVEL_NOTICE, "PASS {}={}".format(api_loan_amt, app_loan_amt))
                else:
                    logger.log(LOGGING_LEVEL_NOTICE, "FAIL {}={}".format(api_loan_amt, app_loan_amt))

                # One record read
                count += 1

            driver.swipe(100, 970, 100, 60, 2000)


    except Exception as ex:
        logger.log(LOGGING_LEVEL_NOTICE,Exception+ str(ex))
        #print("I am in exception")
        #print(ex)

def ui_get_field(driver, path, index):
    try:
        value = (driver.find_element_by_xpath(path + "android.widget.TextView[" + str(index) + "]").text)#.encode('utf8')
        return value
    except Exception as ex:
        print("ui_get_field" + str(ex))
        return None

#@mark.member
def test_get_the_client_sync_api_res(test_setup_backend):
    try:
        res_d = []

        session = Connection.get_session()
        response = session.get(clientsync_url, headers=Connection.get_headers(), data={})
        logger.log(LOGGING_LEVEL_NOTICE,"Response code {} ".format(response.status_code))
        if response.status_code == 200:
            json_obj = json.loads(response.text.encode("utf-8"))

            for d in json_obj.get('data'):
                dic = {}
                dic["id"] = d["id"]
                dic["displayName"] = d["displayName"]
                dic["officeName"] = d["officeName"]
                dic["Status"]= d["clientSummary"]["clientAggregatedStatus"]
                dic["Officer Name"] = d["timeline"]["submittedByFirstname"]+" " +d["timeline"]["submittedByLastname"]

                if (d["clientSummary"]["loanAmount"]  and d["clientSummary"]["currency"] ):
                    dic["loanAmount"] = d["clientSummary"]["currency"]+str(d["clientSummary"]["loanAmount"])+" "+str(d["clientSummary"]["loanType"])+"loan"

                res_d.append(dic)
            print(str(res_d).encode('utf8'))
            return res_d

    except Exception as ex:
        print(ex)