from appium import webdriver
from  pytest import fixture
from util.conn import Connection
from util.config import *
from util.include import *
import json

@fixture(scope="session",params=[(username,password,GRANT_TYPE)])
def test_setup_backend():
    print("test_setup called")
    service_health = eos_url+"admin/health"
    print(service_health)
    response = Connection.get_session().get(service_health, headers=None, data={})
    status = json.loads(response.text).get("status")
    code = response.status_code
    logger.log(LOGGING_LEVEL_NOTICE, "DFA service code {} and status {} ".format(code,status))
    if code ==200 and status== "UP":
        print("I am in ")
        login_url = eos_url + "oauth/token"
        print(login_url)
        payload = 'username=' + username + '&password=' + password + '&grant_type=' + GRANT_TYPE
        response = Connection.get_session().post(login_url, headers=Connection.get_login_headers(), data=payload)

        Connection.set_token(response)
    else :
        print("Failed")
        logger.log(LOGGING_LEVEL_NOTICE, "DFA Service is not up {} ".format(response.text))

@fixture(scope="session")
def test_setup():
    desired_caps = {
        "deviceName": "emulator-5554",
        "platformName": "android",
        "appPackage": "co.gojo.eos",
        "appActivity": ".MainActivity",
        "noReset": True,
       "app": "C:\\GOJO\\APK\\dfa-1.0.0-debug.apk"
    }
    driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_caps)
    driver.implicitly_wait(40)
    yield driver
    driver.close()


