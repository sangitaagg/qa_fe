import pytest
from pytest import mark
from pytest import fixture
import time


'''def test_valid_login(test_setup):
    driver = test_setup

    driver.find_element_by_id("co.gojo.eos:id/editTextUserName").click()
    search_name = driver.find_element_by_id("co.gojo.eos:id/editTextUserName")
    search_name.send_keys("sangita")

    driver.find_element_by_id("co.gojo.eos:id/editTextPassword").click()
    search_password = driver.find_element_by_id("co.gojo.eos:id/editTextPassword")
    search_password.send_keys("sangita")

    driver.find_element_by_id("co.gojo.eos:id/buttonTextView").click()
'''

@fixture(scope= "module")
def test_filter_setup(test_setup):
    driver = test_setup
    driver.find_element_by_id("filtersTextView").click()


@pytest.mark.filter
def test_display_screen_for_Loan(test_setup,test_filter_setup):
    driver = test_setup

    loan_status  = driver.find_element_by_id("loanStatusTextView").text

    assert loan_status == "Loan Status"
    loan_finish_appraisal= driver.find_element_by_id("finishAppraisalChip").text


    assert loan_finish_appraisal == "Finish Appraisal"
    assert driver.find_element_by_id("finishAppraisalChip").is_selected()== False


    loan_rm_check = driver.find_element_by_id("rmCheckChip").text
    assert  loan_rm_check == "RM Check"
    assert  driver.find_element_by_id("rmCheckChip").is_selected() == False

    loan_cm_check= driver.find_element_by_id("cmCheckChip").text
    assert loan_cm_check == "CM Check"
    assert  driver.find_element_by_id("cmCheckChip").is_selected() == False

    loan_disbursed = driver.find_element_by_id("disbursedChip").text
    assert loan_disbursed == "Disbursed"
    assert driver.find_element_by_id("disbursedChip").is_selected() == False

    loan_delinquent = driver.find_element_by_id("delinquentChip").text
    assert loan_delinquent == "Delinquent"
    assert  driver.find_element_by_id("delinquentChip").is_selected() == False

    loan_deactivated = driver.find_element_by_id("deactivatedChip").text
    assert loan_deactivated == "Deactivated"
    assert  driver.find_element_by_id("deactivatedChip").is_selected() == False

    loan_active = driver.find_element_by_id("activeLoanStatusChip").text
    assert loan_active == "Active"
    assert driver.find_element_by_id("activeLoanStatusChip").is_selected() == False




