import pytest
from pytest import mark
from pytest import fixture

@fixture(scope="function")
def test_filter_setup(test_setup):
    driver = test_setup
    driver.find_element_by_id("filtersTextView").click()


@mark.Clientfilter
def test_display_Screen_for_client_status(test_setup,test_filter_setup):
    driver = test_setup
    client_status = driver.find_element_by_id("clientStatusTextView").text
    assert client_status == "Client Status"

    client_active = driver.find_element_by_id("activeClientStatusChip").text
    assert client_active == "Active"
    assert driver.find_element_by_id("activeClientStatusChip").is_selected() == False

    client_closed = driver.find_element_by_id("closedClientStatusChip").text
    assert client_closed == "Closed"
    assert driver.find_element_by_id("closedClientStatusChip").is_selected() == False

    client_rejected = driver.find_element_by_id("rejectedClientStatusChip").text
    assert client_rejected == "Rejected"
    assert driver.find_element_by_id("rejectedClientStatusChip").is_selected() == False

    client_pending = driver.find_element_by_id("pendingClientStatusChip").text
    assert client_pending == "Pending"
    assert driver.find_element_by_id("pendingClientStatusChip").is_selected() == False

@mark.Clientfilter
def test_active_client_filter(test_setup,test_filter_setup):
    driver = test_setup
    client_active = driver.find_element_by_id("activeClientStatusChip").click()
    assert driver.find_element_by_id("activeClientStatusChip").is_selected() == True
    driver.find_element_by_id("confirmButton").click()

    assert driver.find_element_by_id("filterNumberTextView").text == "1"
    assert driver.find_element_by_id("filtersListTextView").text == " Active"
    #TODO : Need to validate the list of active customer
    #TODO : Validate the filter number on filter page
    driver.find_element_by_id("filtersTextView").click()
    driver.find_element_by_id("resetTextView").click()
    driver.find_element_by_id("confirmButton").click()
    #TODO : Need to add the assert in valid exception
    try:
        driver.find_element_by_id("filterNumberTextView")
    except Exception as e:
        print("Exception %S", str(e))

@mark.Clientfilter
def test_closed_client_filter(test_setup,test_filter_setup):
    driver = test_setup
    client_active = driver.find_element_by_id("closedClientStatusChip").click()
    assert driver.find_element_by_id("closedClientStatusChip").is_selected() == True
    driver.find_element_by_id("confirmButton").click()
    assert driver.find_element_by_id("filterNumberTextView").text == "1"
    assert driver.find_element_by_id("filtersListTextView").text == " Closed"
    # TODO : Need to validate the list of active customer
    # TODO : Validate the filter number on filter page
    driver.find_element_by_id("filtersTextView").click()
    driver.find_element_by_id("resetTextView").click()
    driver.find_element_by_id("confirmButton").click()
    # TODO : Need to add the assert in valid exception
    try:
        driver.find_element_by_id("filterNumberTextView")
    except Exception as e:
        print("Exception %S", str(e))

@mark.Clientfilter
def test_rejected_client_filter(test_setup,test_filter_setup):
    driver = test_setup
    client_active = driver.find_element_by_id("rejectedClientStatusChip").click()
    assert driver.find_element_by_id("rejectedClientStatusChip").is_selected() == True
    driver.find_element_by_id("confirmButton").click()
    assert driver.find_element_by_id("filterNumberTextView").text == "1"
    assert driver.find_element_by_id("filtersListTextView").text == " Rejected"
    # TODO : Need to validate the list of active customer
    # TODO : Validate the filter number on filter page
    driver.find_element_by_id("filtersTextView").click()
    driver.find_element_by_id("resetTextView").click()
    driver.find_element_by_id("confirmButton").click()
    # TODO : Need to add the assert in valid exception
    try:
        driver.find_element_by_id("filterNumberTextView")
    except Exception as e:
        print("Exception %S", str(e))

@mark.Clientfilter
def test_pending_client_filter(test_setup,test_filter_setup):
    driver = test_setup
    client_active = driver.find_element_by_id("pendingClientStatusChip").click()
    assert driver.find_element_by_id("pendingClientStatusChip").is_selected() == True
    driver.find_element_by_id("confirmButton").click()
    assert driver.find_element_by_id("filterNumberTextView").text == "1"
    assert driver.find_element_by_id("filtersListTextView").text == " Pending"
    # TODO : Need to validate the list of active customer
    # TODO : Validate the filter number on filter page
    driver.find_element_by_id("filtersTextView").click()
    driver.find_element_by_id("resetTextView").click()
    driver.find_element_by_id("confirmButton").click()
    # TODO : Need to add the assert in valid exception
    try:
        driver.find_element_by_id("filterNumberTextView")
    except Exception as e:
        print("Exception %S", str(e))

@mark.Clientfilter
def test_active_pending_filter_combination(test_setup,test_filter_setup):
    driver = test_setup
    driver.find_element_by_id("activeClientStatusChip").click()
    driver.find_element_by_id("pendingClientStatusChip").click()

    assert driver.find_element_by_id("pendingClientStatusChip").is_selected() == True
    assert driver.find_element_by_id("activeClientStatusChip").is_selected() == True
    driver.find_element_by_id("confirmButton").click()
    assert driver.find_element_by_id("filterNumberTextView").text == "2"
    assert driver.find_element_by_id("filtersListTextView").text == " Active,  Pending"
    # TODO : Need to validate the list of active customer
    # TODO : Validate the filter number on filter page
    driver.find_element_by_id("filtersTextView").click()
    driver.find_element_by_id("resetTextView").click()
    driver.find_element_by_id("confirmButton").click()
    # TODO : Need to add the assert in valid exception
    try:
        driver.find_element_by_id("filterNumberTextView")
    except Exception as e:
        print("Exception %S", str(e))

@mark.Clientfilter
def test_rejected_closed_filter_combination(test_setup,test_filter_setup):
    driver = test_setup
    driver.find_element_by_id("rejectedClientStatusChip").click()
    driver.find_element_by_id("closedClientStatusChip").click()
    assert driver.find_element_by_id("rejectedClientStatusChip").is_selected() == True
    assert driver.find_element_by_id("closedClientStatusChip").is_selected() == True
    driver.find_element_by_id("confirmButton").click()
    assert driver.find_element_by_id("filterNumberTextView").text == "2"
    assert driver.find_element_by_id("filtersListTextView").text == " Rejected,  Closed"
    # TODO : Need to validate the list of active customer
    # TODO : Validate the filter number on filter page
    driver.find_element_by_id("filtersTextView").click()
    driver.find_element_by_id("resetTextView").click()
    driver.find_element_by_id("confirmButton").click()
    # TODO : Need to add the assert in valid exception
    try:
        driver.find_element_by_id("filterNumberTextView")
    except Exception as e:
        print("Exception %S", str(e))


