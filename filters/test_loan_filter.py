import pytest
from pytest import mark
from pytest import fixture
import time


fixture(scope= "module")
def test_filter_setup(test_setup):
    driver = test_setup
    driver.find_element_by_id("filtersTextView").click()

@pytest.mark.filter
def test_display_screen_for_Loan(test_setup,test_filter_setup):
    driver = test_setup

    loan_status  = driver.find_element_by_id("loanStatusTextView").text

    assert loan_status == "Loan Status"
    loan_finish_appraisal= driver.find_element_by_id("loanInvalidChip").text


    assert loan_finish_appraisal == "Loan Invalid"
    assert driver.find_element_by_id("finishAppraisalChip").is_selected()== False

    loan_rm_check = driver.find_element_by_id("loanPendingApprovalChip").text
    assert  loan_rm_check == "Loan Pending Approval"
    assert  driver.find_element_by_id("loanPendingApprovalChip").is_selected() == False

    loan_cm_check= driver.find_element_by_id("loanApprovedChip").text
    assert loan_cm_check == "Loan Approved"
    assert  driver.find_element_by_id("loanApprovedChip").is_selected() == False

    loan_disbursed = driver.find_element_by_id("loanActiveChip").text
    assert loan_disbursed == "Active"
    assert driver.find_element_by_id("loanActiveChip").is_selected() == False

    loan_delinquent = driver.find_element_by_id("loanTransferInProgressChip").text
    assert loan_delinquent == "Loan Transfer in Progress"
    assert  driver.find_element_by_id("loanTransferInProgressChip").is_selected() == False

    loan_deactivated = driver.find_element_by_id("loanTransferRejectedChip").text
    assert loan_deactivated == "Loan transfer on Hold"
    assert  driver.find_element_by_id("loanTransferRejectedChip").is_selected() == False

    loan_active = driver.find_element_by_id("applicationWithdrawnChip").text
    assert loan_active == "Application Withdrawn"
    assert driver.find_element_by_id("applicationWithdrawnChip").is_selected() == False

    loan_active = driver.find_element_by_id("loanRejectedChip").text
    assert loan_active == "Loan Rejected"
    assert driver.find_element_by_id("loanRejectedChip").is_selected() == False

    loan_active = driver.find_element_by_id("loanRepaidClosedChip").text
    assert loan_active == "Loan Fully Repaid and Closed"
    assert driver.find_element_by_id("loanRepaidClosedChip").is_selected() == False

    loan_active = driver.find_element_by_id("loanWrittenOffChip").text
    assert loan_active == "Loan Written Off"
    assert driver.find_element_by_id("loanWrittenOffChip").is_selected() == False

    loan_active = driver.find_element_by_id("loanRescheduledChip").text
    assert loan_active == "Loan Rescheduled"
    assert driver.find_element_by_id("loanRescheduledChip").is_selected() == False

    loan_active = driver.find_element_by_id("loanOverpaidChip").text
    assert loan_active == "Overpaid"
    assert driver.find_element_by_id("loanOverpaidChip").is_selected() == False


def test_loan_invalid_filter(test_setup,test_filter_setup):
    driver = test_setup
    driver.find_element_by_id("loanInvalidChip").click()
    assert driver.find_element_by_id("loanInvalidChip").is_selected() == True
    driver.find_element_by_id("confirmButton").click() #Confirm box
    assert driver.find_element_by_id("filterNumberTextView").text == "1"
    assert driver.find_element_by_id("filtersListTextView").text == " Loan Invalid"
    driver.find_element_by_id("filtersTextView").click()
    driver.find_element_by_id("resetTextView").click()
    driver.find_element_by_id("confirmButton").click()

    try:
        driver.find_element_by_id("filterNumberTextView")
    except Exception as e:
        print("Exception %s", str(e))

def test_loan_Pending_approval_filter(test_setup,test_filter_setup):
    driver = test_setup
    validate_single_filter(driver, "loanPendingApprovalChip", "Loan Pending Approval")

def test_loan_approved_filter(test_setup,test_filter_setup):
    driver = test_setup
    validate_single_filter(driver, "loanApprovedChip", "Loan Approved")

def test_loan_active_filter(test_setup,test_filter_setup): #TODO need to check if its valid filter or not
    driver = test_setup
    validate_single_filter(driver, "loanActiveChip", "Active")

def test_loan_transfer_in_progress(test_setup,test_filter_setup):
    driver = test_setup
    validate_single_filter(driver, "loanTransferInProgressChip", "Loan Transfer in Progress")

def test_loan_transfer_on_hold(test_setup,test_filter_setup):
    driver = test_setup
    validate_single_filter(driver, "loanTransferRejectedChip", "Loan Transfer in Progress")

def test_loan_application_withdrawn(test_setup,test_filter_setup):
    driver = test_setup
    validate_single_filter(driver, "applicationWithdrawnChip", "Application Withdrawn")


def test_loan_rejected(test_setup,test_filter_setup):
    driver = test_setup
    validate_single_filter(driver, "loanRejectedChip", "Loan Rejected")

def test_loan_fully_repaid_and_closed(test_setup,test_filter_setup):
    driver = test_setup
    validate_single_filter(driver, "loanRepaidClosedChip", "Loan Fully Repaid and Closed")

def test_written_off(test_setup,test_filter_setup):
    driver = test_setup
    validate_single_filter(driver, "loanWrittenOffChip", "Loan Written Off")


def test_loan_resheduled(test_setup,test_filter_setup):
    driver = test_setup
    validate_single_filter(driver, "loanRescheduledChip", "Loan Rescheduled")

def test_overpaid(test_setup,test_filter_setup):
    driver = test_setup
    validate_single_filter(driver,"loanOverpaidChip","Overpaid")

def test_three_loan_filter(test_setup,test_filter_setup):
    driver = test_setup
    #TODO : Need to see how combination of filter work
    driver.find_element_by_id("loanPendingApprovalChip").click()
    driver.find_element_by_id("loanApprovedChip").click()
    driver.find_element_by_id("loanActiveChip").click()
    driver.find_element_by_id("confirmButton").click()
    assert driver.find_element_by_id("filterNumberTextView").text == "3"

def validate_single_filter(driver,s_filter,result,num_of_filter="1"):
    driver.find_element_by_id(s_filter).click()
    assert driver.find_element_by_id(s_filter).is_selected() == True
    driver.find_element_by_id("confirmButton").click()  # Confirm box
    assert driver.find_element_by_id("filterNumberTextView").text == num_of_filter
    assert driver.find_element_by_id("filtersListTextView").text == result
    driver.find_element_by_id("filtersTextView").click()
    driver.find_element_by_id("resetTextView").click()
    driver.find_element_by_id("confirmButton").click()

    try:
        driver.find_element_by_id("filterNumberTextView")
    except Exception as e:
        print("Exception %s", str(e))

